var x, i, j, l, ll, selElmnt, a, b, c;

x = document.getElementsByClassName("nuevo");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;

  a = document.createElement("div");
 
  a.setAttribute("class", "seleccionado");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  
  x[i].appendChild(a);
  b = document.createElement("div");
  b.setAttribute("class", "opciones esconder");
  
  for (j = 1; j < ll; j++) {

    c = document.createElement("div");
    /*var p = document.createElement("p");
         p.setAttribute("class", "opcion");
         c.appendChild(p); */
    c.innerHTML = selElmnt.options[j].innerHTML;
   
   
        
    c.addEventListener("click", function(e) {

        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("elegida");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "elegida");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {

      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("esconder");
      this.classList.toggle("flecha");
    });
}
function closeAllSelect(elmnt) {

  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("opciones");
  y = document.getElementsByClassName("seleccionado");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("flecha");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("esconder");
    }
  }
}

document.addEventListener("click", closeAllSelect);